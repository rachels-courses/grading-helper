echo "ARGUMENT 1: Path to code, ARGUMENT 2: .txt file of inputs"

path=$1
inputs=$2
remove="file://"         # Remove the file:// prefix that gets copied when I copy the path
path=${path#"$remove"}
exe=$path/a.out

echo "PATH: $path"

outfile="$path/report.html"
echo "REPORT: $outfile"
echo $1 > $outfile

# ---------------------------------------------------------------------- BUILD PROGRAM
echo "BUILD PROGRAM"
echo "g++ $path/*.cpp $path/*.h -o $exe"
g++ $path/*.cpp $path/*.h -o $exe
g++ $path/*.cpp $path/*.hpp -o $exe
g++ $path/*.cpp -o $exe

cat template_webpage_00_head.html >> $outfile

# ---------------------------------------------------------------------- PROGRAM EXECUTION
cat template_webpage_01_programout.html >> $outfile
cat $inputs | while read line
do
  echo "TEST: $line"
  echo "<p>$exe $line</p><pre class='sourcefile'>" >> $outfile
  #echo $line | $exe >> $outfile
  echo "</pre>" >> $outfile
done 

# ---------------------------------------------------------------------- SOURCE CODE
cat template_webpage_02_sourcecode.html >> $outfile
echo "WRITE SOURCE CODE TO REPORT"
for f in $path/*.h    # For each .h file in the directory
do  
  echo "<p>$f</p><pre>" >> $outfile
  cat $f >> $outfile
  echo "</pre>" >> $outfile
done

for f in $path/*.hpp    # For each .hpp file in the directory
do  
  echo "<p>$f</p><pre>" >> $outfile
  cat $f >> $outfile
  echo "</pre>" >> $outfile
done

for f in $path/*.cpp    # For each .cpp file in the directory
do  
  echo "<p>$f</p><pre>" >> $outfile
  cat $f >> $outfile
  echo "</pre>" >> $outfile
done

cat template_webpage_03_foot.html >> $outfile

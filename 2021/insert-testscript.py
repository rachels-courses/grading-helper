import subprocess
import sys
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def SanitizePath( path ):
    return path.replace( " ", "\ " )

def GetSubfolders( parentFolder ):
    subfolders = [f.path for f in os.scandir( parentFolder ) if f.is_dir()]
    return subfolders

def FindFoldersWithName( basePath, folderName ):
    folderPaths = []
    # From https://stackoverflow.com/questions/47197463/how-to-search-for-a-folder-name-in-python-and-delete-it
    for root, subdirs, files in os.walk( basePath ):
        for d in subdirs:
            if ( d == folderName ):                
                projectPath = os.path.join( root, d )
                folderPaths.append( projectPath )
    return folderPaths
    
def CopyScriptToFolders( studentFolders, assignment, scriptPath ):
    print( "\n * insert-testscript.py - CopyScriptToFolders" )
    projectFolders = FindFoldersWithName( path, assignment )
    
    for folder in projectFolders:
        print( "\n * insert-testscript.py - Copying script into folder [[" + folder + "]]..." )
        
        print( "\n * folder: [[" + folder + "]]" )
        print( "\n * scriptPath: [[" + scriptPath + "]]" )
        
        command = "cp " + SanitizePath( scriptPath ) + " " + SanitizePath( folder ) + "/testscript.py"
        print( "\n * insert-testscript.py - COMMAND: [[" + command + "]]" )
        os.system( command )

# Program begins

print( "" )
print( "ARGUMENTS" )
for i in range( len( sys.argv ) ):
    print( str( i ) + ": " + sys.argv[i], end="\t" )
print( "\n" )

if ( len( sys.argv ) < 4 ):
    print( "Format: python3     insert-testscript.py  class  assignment  testscript-path" )
    print( "Example:    python3" )
    print( "script:     insert-testscript.py" )
    print( "class:      cs200-350" )
    print( "assignment: 02Be\ -\ Console\ Input\ and\ Output" )
    print( "scriptpath: /home/wilsha/RACHEL/_ADULTING/TEACHING/2021-01_SPRING/cs200-student/Unit\ 02\ -\ Input,\ output,\ storing\ data,\ and\ doing\ math/02Be\ -\ Console\ Output" )
    
    print( "" )
    
    #path            = input( "Enter class..........." )
    #assignment      = input( "Enter assignment......" )
    #scriptPath      = input( "Enter script path....." )
    path            = "cs200-377"
    assignment      = "03Be - Branching with If Statements"
    scriptPath      = "/home/wilsha/RACHEL/_ADULTING/TEACHING/2021-01_SPRING/cs200-student/Unit 03 - Control flow - Boolean logic and branching with if statements and switch statements/03Be - Branching with If Statements/testscript.py"

else:
    path            = sys.argv[1]
    assignment      = sys.argv[2]
    scriptPath      = sys.argv[3]
    

path = os.path.join( "student-repos", path )
studentFolders  = GetSubfolders( path )

CopyScriptToFolders( studentFolders, assignment, scriptPath )

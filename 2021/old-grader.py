import subprocess
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

logger = open( "log.txt", "w" )

path = "/home/wilsha/RACHEL/_ADULTING/TEACHING/2021-01_SPRING/grading-helper/student-repos/"

def Log( text ):
    print( text )
    logger.write( text + "\n" )
    
def LogL( label, text ):
    print( label + " = " + text )
    logger.write( label + " = " + text + "\n" )

def GetSubfolders(parentFolder):
    subfolders = [f.path for f in os.scandir( parentFolder ) if f.is_dir()]
    return subfolders

def PullLatestFromRepositories(studentFolders):
    for student in studentFolders:
        print("Pulling latest in " + folder + "...")

def GradingBreakdown( student, assignment, studentPath, assignmentPath, outputPath, sourcePaths ):
    
    studentGradeFile = student + " - " + assignment + ".txt"
    gradePath = os.path.join( studentPath, studentGradeFile )
    breakdown = open( gradePath, "w" )
    
    breakdown.write( "ASSIGNMENT: \n" + assignment + "\n\n" )
    breakdown.write( "STUDENT: \n" + student + "\n\n" )
    breakdown.write( "PROGRAM OUTPUT:\n" )

    fileout = open( outputPath, "r" )
    breakdown.write( fileout.read() )
    fileout.close()
    
    breakdown.write( "\n" )
    
    for sourcePath in sourcePaths:
        sourceName = sourcePath.replace( assignmentPath + "/", "" )
        fileout = open( sourcePath, "r" )
        
        breakdown.write( "FILE: " + sourceName + " \n\n" );
        breakdown.write( fileout.read() )
        breakdown.write( "\n\n" )
        fileout.close()
        
    breakdown.close()

def BuildAllAssignments(basePath, studentFolders):
    for student in studentFolders:
        studentName = student.replace( basePath, "" )
        LogL( "- STUDENT", studentName )
        studentPath = os.path.join( basePath, student )
        assignments = GetSubfolders( studentPath )
        
        # Look at each assignment
        for assignment in assignments:
            assignmentName = assignment.replace( studentPath + "/", "" )
            LogL( "-- ASSIGNMENT", assignmentName )

            # What source files are in here?
            assignmentPath = os.path.join( basePath, student, assignment )
            LogL( "--- assignmentPath", assignmentPath )
            sourceFiles = []
            for root, dirs, files in os.walk(assignmentPath):
               for file in files:
                   if (file.endswith(".cpp") or file.endswith(".hpp") or file.endswith(".h")):
                       sourceFiles.append(file)
            
            # Build these source files
            buildCommand = "g++ "
            outputPath = os.path.join( assignmentPath, "output.o" )
            sourcePaths = []
            for file in sourceFiles:
                sourcePath = os.path.join( assignmentPath, file )
                sourcePaths.append( sourcePath )
                buildCommand += "\"" + sourcePath + "\"" + " "
            buildCommand += " -o \"" + outputPath + "\""
            LogL( "--- buildCommand", buildCommand )
            os.system(buildCommand)
            
            # Run the tests
            testerPath = os.path.join( assignmentPath, "tester.sh" )
            testCommand = "sh \"" + testerPath + "\""
            LogL( "--- testCommand", testCommand )
            os.system( testCommand )
            
            runOutputPath = os.path.join( assignmentPath, "output.txt" )
            
            # Create a grading breakdown
            GradingBreakdown( studentName, assignmentName, studentPath, assignmentPath, runOutputPath, sourcePaths )
            


Log("GRADING HELPER START")
LogL( "path", path )
studentFolders = GetSubfolders( path )
# Pull Latest
BuildAllAssignments(path, studentFolders)

import subprocess
import sys
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def SanitizePath( path ):
    return path.replace( " ", "\ " )
    
def LinkizePath( path ):
    return path.replace( " ", "-" )

def GetSubfolders( parentFolder ):
    subfolders = [f.path for f in os.scandir( parentFolder ) if f.is_dir()]
    return subfolders
    
def FindFoldersWithName( basePath, folderName ):
    folderPaths = []
    # From https://stackoverflow.com/questions/47197463/how-to-search-for-a-folder-name-in-python-and-delete-it
    for root, subdirs, files in os.walk( basePath ):
        for d in subdirs:
            if ( d == folderName ):                
                projectPath = os.path.join( root, d )
                folderPaths.append( projectPath )
    return folderPaths
      
def FindFile( currentPath, fileText ):
    foundFiles = []
    for root, subdirs, files in os.walk( currentPath ):
        for f in files:
            if ( f.find( fileText ) != -1 ):                
                foundFiles.append( os.path.join( root, f ) )
    return foundFiles
      
def RunTestScript( programPath, projectPath ):
    print( "\n * grader.py - RUN TEST SCRIPT" )
    scriptPath = os.path.join( programPath, projectPath )
    scriptPath = os.path.join( scriptPath, "testscript.py" )
    
    print( "\n * grader.py - Running testscript for: [[" + projectPath + "]]..." )
    
    scriptPath = SanitizePath( scriptPath )
    arguments = " " + SanitizePath( projectPath )
    command = "python3 " + scriptPath + arguments
    print( "\n * grader.py - COMMAND: [[" + command + "]]" )
    os.system( command )
    
def GetStudentName( path, project ):
    name = project.replace( path + "/", "" )
    slash = name.find( "/" )
    name = name[0:slash]
    return name

def GetProgramOutput( projectPath ):
    outputPath = os.path.join( projectPath, "output.txt" );
    
    try:
        outputFile = open( outputPath, "r" )
        contents = outputFile.read()
    except:
        contents = "EXCEPTION ENCOUNTERED DURING READ!!"
            
    return contents
    
def GetSourceCode( srcPath ):
    
    try:
        outputFile = open( srcPath, "r" )
        contents = outputFile.read()
    except:
        contents = "EXCEPTION ENCOUNTERED DURING READ!!"
        
    return contents

def AssembleReport( programPath, projectPath, fileout ):
    print( "\n * grader.py - ASSEMBLE REPORT" )
    
    outputPath = os.path.join( projectPath, "output.txt" );
    contents = GetProgramOutput( projectPath )
    
    fileout.write( "<h4>Program output</h4>\n" )
    fileout.write( "<pre>" );
    fileout.write( contents )
    fileout.write( "</pre>" );
    
    fileout.write( "<h4>Program code</h4>\n" )
    
    sourceFiles = FindFile( projectPath, ".hpp" )
    for source in sourceFiles:
        contents = GetSourceCode( source )        
        fileout.write( "<h5>" + source + "</h5>\n" )
        fileout.write( "<pre><code>" )
        fileout.write( contents )
        fileout.write( "</code></pre>\n" )
        
    sourceFiles = FindFile( projectPath, ".cpp" )    
    for source in sourceFiles:
        contents = GetSourceCode( source )       
        fileout.write( "<h5>" + source + "</h5>\n" )
        fileout.write( "<pre><code>" )
        fileout.write( contents )
        fileout.write( "</code></pre>\n" )
        
    
def StartHtml( fileout, assignment ):
    fileout.write( "<!DOCTYPE html>\n" )
    fileout.write( "<html lang='en'><head><meta charset='utf-8'>" )
    fileout.write( "<title>" + assignment + "</title>\n" )
    fileout.write( "<script src='https://code.jquery.com/jquery-3.5.1.min.js' integrity='sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=' crossorigin='anonymous'></script> \n" )
    fileout.write( "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'> \n" )
    fileout.write( "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' integrity='sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6' crossorigin='anonymous'></script> \n" )
    fileout.write( "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css'> \n" )
    fileout.write( "<script src='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js'></script> \n" )
    fileout.write( "<script>hljs.initHighlightingOnLoad();</script> \n" )
    fileout.write( "<style> \n" )
    fileout.write( "#table-container { max-width: 1280px; } \n" )
    fileout.write( "table .count    { width: 2%; \n }" )
    fileout.write( "table .student  { width: 8%; \n }" )
    fileout.write( "table .output1  { width: 35%; \n }" )
    fileout.write( "table .output2  { width: 35%; \n }" )
    fileout.write( "</style> \n" )
    fileout.write( "</head>\n<body>\n" )
    fileout.write( "<div class='container-fluid'> \n" )
    
def EndHtml( fileout ):
    fileout.write( "</div>\n" )
    fileout.write( "</body></html> \n" )
    
# Program begins

# Arguments:  sys.argv
# Expected arguments: python3 grader.py class assignment

print( "" )
print( "grader.py" )
print( "" )
print( "ARGUMENTS" )
for i in range( len( sys.argv ) ):
    print( str( i ) + ": " + sys.argv[i], end="\t" )
print( "" )

if ( len( sys.argv ) < 3 ):
    print( "Format: python3 grader.py class assignment" )
    print( "Example: python3 grader.py cs235 02Be\ -\ Testing" )
    
    coursePath      = "cs200-376"
    assignment      = "03Be - Branching with If Statements"
    #assignment      = "02Be - Console Input and Output"

else:
    coursePath = sys.argv[1]
    assignment = sys.argv[2]
    
programPath = os.path.abspath( os.path.dirname( sys.argv[0] ) )
print( "\n * grader.py - programPath: [[" + programPath + "]]" )

path = os.path.join( "student-repos", coursePath )

fileout = open( "report_" + assignment + "_" + coursePath + ".html", "w" )
StartHtml( fileout, assignment )

studentFolders = GetSubfolders( path )

projectFolders = FindFoldersWithName( path, assignment )

fileout.write( "<hr>\n" );
fileout.write( "<h2>Results</h2>\n" );

counter = 0

fileout.write( "<div id='table-container'><table class='table table-striped' style='max-width:1280px;'><tr> \n" )
fileout.write( "<th class='count'  >#</th> \n" )
fileout.write( "<th class='student'>Student</th> \n" )
fileout.write( "<th class='output1'>Program output</th> \n" )
fileout.write( "<th class='output2'>Source code</th> \n" )
fileout.write( "</tr>" )

for project in projectFolders:
    print( "" )
    for i in range( 160 ): print( "-", end="" )
    print( "\n * grader.py - START " + project )

    RunTestScript( programPath, project )

    sourceFilesHpp = FindFile( project, ".hpp" )
    sourceFilesCpp = FindFile( project, ".cpp" )
    
    allSource = sourceFilesHpp + sourceFilesCpp
    allSource.sort()
    
    fileout.write( "<tr> \n" )
    fileout.write( "<td class='count'  >" + str( counter ) + "</td> \n" ) # #
    fileout.write( "<td class='student'>" + GetStudentName( path, project ) + "</td> \n" ) # Student
    fileout.write( "<td class='output1'><pre>" + GetProgramOutput( project ) + "</pre></td> \n" ) # Output
    fileout.write( "<td class='output2'> \n" )
    
    for src in allSource:
        if ( "utilities" in src ): continue
        fullpath = os.path.join( programPath, src )
        contents = GetSourceCode( src )
        lastSlash = fullpath.rfind( "/" ) + 1
        prettyHeader = fullpath[lastSlash:]
        uid = prettyHeader.replace( ".", "" ) + str( counter )
        color = "btn-success"
        if ( "hpp" in src ): color = "btn-warning"
        
        fileout.write( '<p><a class="btn ' + color + '" data-toggle="collapse" href="#' + uid + '" role="button" aria-expanded="false" aria-controls="' + uid + '">' + prettyHeader + '</a></p>' )
        fileout.write( '<div class="collapse" id="' + uid + '">' )
        fileout.write( '  <div class="card card-body">' )
        fileout.write( '<pre><code>' )
        fileout.write( contents )
        fileout.write( '</code></pre>' )
        fileout.write( '  </div>' )
        fileout.write( '</div>' )
        
    fileout.write( "</td> \n" ) # Src
    fileout.write( "</tr> \n" )
    
    counter = counter + 1
    
    print( "\n * grader.py - END " + project )
    for i in range( 160 ): print( "-", end="" )
    print( "" )
    
fileout.write( "</table></div>" )

EndHtml( fileout )

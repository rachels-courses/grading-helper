import subprocess
import sys
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def GetSubfolders( parentFolder ):
    subfolders = [f.path for f in os.scandir( parentFolder ) if f.is_dir()]
    return subfolders

def CloneRepositories( repoList, path ):
    print( "" )
    for line in repoList.readlines():
        line = line.strip()
        info = line.split( " " )
        repo = info[0]
        #print( "Info:", info )
        
        fullPath = os.path.join( path, info[1] )
        #print( "Path: ", fullPath )
        
        command = "git clone " + repo + " " + fullPath
        
        #print( "Command:",  command )
        os.system( command )


# Program begins

print( "" )
print( "ARGUMENTS" )
for i in range( len( sys.argv ) ):
    print( str( i ) + ": " + sys.argv[i], end="\t" )
print( "" )

if ( len( sys.argv ) < 3 ):
    print( "Format:  python3 grader.py file path" )
    print( "Example: python3 grader.py repo-list-cs200-350.txt student-repos/cs200" )
    

repoList = open( sys.argv[1], "r" )
path = sys.argv[2]

CloneRepositories( repoList, path )

#include <iostream>
using namespace std;

int main()
{
    cout << "Enter two numbers: ";
    int a, b;
    cin >> a >> b;
    
    int result = a + b;
    
    cout << a << " + " << b << " = " << result << endl;
    
    return 0;
}

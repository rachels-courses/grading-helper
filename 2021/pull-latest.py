import subprocess
import sys
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def GetSubfolders( parentFolder ):
    subfolders = [f.path for f in os.scandir( parentFolder ) if f.is_dir()]
    return subfolders

def PullLatestFromRepositories( studentFolders ):
    for student in studentFolders:
        print( "\t * pull-latest.py - FOLDER \"" + student + "\"..." )
        
        command = "git -C " + student + " stash >> gitlog.txt"
        os.system( command )
        
        command = "git -C " + student + " pull >> gitlog.txt"
        os.system( command )

# Program begins

print( "" )
print( "ARGUMENTS" )
for i in range( len( sys.argv ) ):
    print( str( i ) + ": " + sys.argv[i], end="\t" )
print( "" )

if ( len( sys.argv ) < 2 ):
    print( "Format: python3 pull-latest.py class" )
    print( "Example: python3 pull-latest.py cs235" )
    

path = os.path.join( "student-repos", sys.argv[1] )
studentFolders = GetSubfolders( path )
print( "\n * pull-latest.py - PULL LATEST FROM \"" + path + "\"..." )

PullLatestFromRepositories( studentFolders )
